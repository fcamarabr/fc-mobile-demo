(function() {
  angular.module('ionicApp.controllers', [])
    .controller('NavCtrl', function($scope, $ionicSideMenuDelegate) {
      $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
      };
      $scope.showRightMenu = function() {
        $ionicSideMenuDelegate.toggleRight();
      };
    })
    .controller('HomeTabCtrl', function($scope) {

    })
    .controller('MapCtrl', function($scope, uiGmapGoogleMapApi, $cordovaGeolocation) {
      $scope.map = {
        center: {
          latitude: 45,
          longitude: -73
        },
        zoom: 18
      };

      uiGmapGoogleMapApi.then(function(maps) {
        $cordovaGeolocation
          .getCurrentPosition({timeout: 10000, enableHighAccuracy: true})
          .then(function (position) {
            $scope.map.center = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              };
          }, function(err) {
            console.error(err);
          });
      });

    })
    .controller('GraficosCtrl', function($scope) {
      $scope.labels = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho"];
      $scope.series = ['App1', 'App2'];
      $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
      ];
    })
    .controller('BarcodeCtrl', function($scope, $cordovaBarcodeScanner) {

      document.addEventListener("deviceready", function() {

        $cordovaBarcodeScanner
          .scan()
          .then(function(barcodeData) {
            $scope.valor = barcodeData;
          }, function(error) {
            $scope.valor = error;
          });

      }, false);
    })
    .controller('PushCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaLocalNotification) {
      $ionicPlatform.ready(function() {
        $scope.push = function() {
          $cordovaLocalNotification.schedule({
            id: 1,
            title: 'FC Mobile Demo',
            text: $scope.mensagem || "Teste de notificação",
            data: {
              customProperty: 'custom value'
            }
          }).then(function(result) {
            // ...
          });
        };
      });
    })
    .controller('ListCtrl', function MyController($scope, $http) {
      $scope.items = [];
      $scope.start = 1;
      $scope.end = 10;
      $scope.loadMore = function() {
        $http.get('https://api.github.com/users/mralexgray/repos?page=' + $scope.start + '&per_page=10' + $scope.end).then(function(items) {
          $scope.items = $scope.items.concat(items.data);
          $scope.start += 1;
          $scope.$broadcast('scroll.infiniteScrollComplete');
        });
      };
      $scope.moreDataCanBeLoaded = function() {
        return $scope.start < 5;
      }
      $scope.$on('$stateChangeSuccess', function() {
        $scope.loadMore();
      });
    });
})();
