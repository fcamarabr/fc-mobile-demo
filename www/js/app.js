(function() {
  angular
    .module('ionicApp', [
      'ionic',
      'ionicApp.controllers',
      'chart.js',
      'ngCordova',
      'uiGmapgoogle-maps'
    ])
    .config(function(uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
        //key: 'your api key',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
      });
    })
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('search', {
          url: '/search',
          templateUrl: 'templates/search.html'
        })
        .state('settings', {
          url: '/settings',
          templateUrl: 'templates/settings.html'
        })
        .state('mapas', {
          url: '/mapas',
          templateUrl: 'templates/geolocalizacao.html',
          controller: 'MapCtrl'
        })
        .state('barcode', {
          url: '/barcode',
          templateUrl: 'templates/barcode.html',
          controller: 'BarcodeCtrl'
        })
        .state('graficos', {
          url: '/graficos',
          templateUrl: 'templates/graficos.html',
          controller: 'GraficosCtrl'
        })
        .state('push', {
          url: '/push',
          templateUrl: 'templates/push.html',
          controller: 'PushCtrl'
        })
        .state('tabs', {
          url: "/tab",
          abstract: true,
          templateUrl: "templates/tabs.html"
        })
        .state('tabs.home', {
          url: "/home",
          views: {
            'home-tab': {
              templateUrl: "templates/home.html",
              controller: 'HomeTabCtrl'
            }
          }
        })
        .state('tabs.graficos', {
          url: '/graficos',
          views: {
            'graficos-tab': {
              templateUrl: 'templates/graficos.html',
              controller: 'GraficosCtrl'
            }
          }
        })
        .state('tabs.infinite', {
          url: '/infinite',
          views: {
            'contatos-tab': {
              templateUrl: 'templates/infinite.html',
              controller: 'ListCtrl'
            }
          }
        })
        .state('tabs.facts2', {
          url: "/facts2",
          views: {
            'home-tab': {
              templateUrl: "templates/facts2.html"
            }
          }
        })
        .state('tabs.about', {
          url: "/about",
          views: {
            'about-tab': {
              templateUrl: "templates/about.html"
            }
          }
        })
        .state('tabs.navstack', {
          url: "/navstack",
          views: {
            'about-tab': {
              templateUrl: "templates/nav-stack.html"
            }
          }
        })
        .state('tabs.contact', {
          url: "/contact",
          views: {
            'contact-tab': {
              templateUrl: "templates/contact.html"
            }
          }
        });


      $urlRouterProvider.otherwise("/tab/home");

    });

})();
